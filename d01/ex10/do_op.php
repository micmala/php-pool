#!/usr/bin/php
<?php
function calculate($argv) {
	switch ($argv[2]) {
	case "+":
		echo ($argv[1] + $argv[3]), "\n";
		break;
	case '-':
		echo ($argv[1] - $argv[3]), "\n";
		break;
	case '*':
		echo ($argv[1] * $argv[3]), "\n";
		break;
	case '/':
		if ($argv[3] != 0) {
			echo ($argv[1] / $argv[3]), "\n";
		}
		break;
	case '%':
		if ($argv[3] != 0) {
			echo ($argv[1] % $argv[3]), "\n";
		}
		break;
	default:
		break;
	}
}

if ($argc != 4) {
	exit("Incorrect parameters\n");
}

for ($i = 1; $i < $argc; $i++) {
	$argv[$i] = trim($argv[$i]);
}

if (is_numeric($argv[1]) && is_numeric($argv[3])) {
	calculate($argv);
}

?>