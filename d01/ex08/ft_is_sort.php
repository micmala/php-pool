<?php
function ft_is_sort($tab) {
	if (!$tab) {
		return false;
	}

	$len = count($tab);
	if ($len > 2) {
		for ($i = 0; $i < $len - 1; $i++) {
			if (strcmp($tab[$i], $tab[$i + 1]) > 0) {
				return false;
			}
		}
	}
	return true;
}

?>