#!/usr/bin/php
<?php

function odd_even($value) {
	print("The number $value is ");
	if ($value % 2 == 0) {
		print("even");
	} else {
		print("odd");
	}

}

print("Enter a number: ");
while ($line = fgets(STDIN)) {
	$line = str_replace("\n", "", trim($line));
	if (is_numeric($line)) {
		odd_even($line);
	} else {
		echo '\'' . $line . '\' is not a number';
	}

	echo "\n";
	print("Enter a number: ");
}

?>