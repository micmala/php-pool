#!/usr/bin/php
<?php

function ft_split($string) {
	if ($string) {
		$string = preg_replace('!\s+!', ' ', trim($string));
		$result = explode(' ', $string);
		sort($result);
		return $result;
	}
}

$result = array();
for ($i = 1; $i < $argc; $i++) {
	$curr = ft_split($argv[$i]);
	$result = array_merge($result, $curr);
}

sort($result);

foreach ($result as $elem) {
	echo "$elem\n";
}

?>