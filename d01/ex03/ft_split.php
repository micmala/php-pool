<?php

function ft_split($string) {
	if ($string) {
		$string = preg_replace('!\s+!', ' ', trim($string));
		$result = explode(' ', $string);
		sort($result);
		return $result;
	}
}

?>