#!/usr/bin/php
<?php

if ($argc != 2) {
	exit("Incorrect parameters\n");
}

$pos = 0;

function calculate($argv) {
	switch ($argv[1]) {
	case "+":
		echo ($argv[0] + $argv[2]), "\n";
		break;
	case '-':
		echo ($argv[0] - $argv[2]), "\n";
		break;
	case '*':
		echo ($argv[0] * $argv[2]), "\n";
		break;
	case '/':
		if ($argv[2] != 0) {
			echo ($argv[0] / $argv[2]), "\n";
		}
		break;
	case '%':
		if ($argv[2] != 0) {
			echo ($argv[0] % $argv[2]), "\n";
		}
		break;
	default:
		break;
	}
}

function get_arg($str) {
	$res;
	global $pos;
	$i = 0;
	if (($str[$pos] == '+' || $str[$pos] == '-') && ctype_digit($str[$pos + 1])) {
		$res .= $str[$pos];
		$i++;
		$pos++;
	}
	while (ctype_digit($str[$pos])) {
		$res .= $str[$pos++];
	}
	return ($res);
}

function get_op($string) {
	global $pos;
	return ($string[$pos++]);
}

function format($string) {
	$trimmed = preg_replace('!\s+!', '', trim($string));
	$tab = array();
	$tab[0] = get_arg($trimmed);
	$tab[1] = get_op($trimmed);
	$tab[2] = get_arg($trimmed);
	return ($tab);
}

$tab = format($argv[1]);
if (is_numeric($tab[0]) && strchr($tab[1], "+-*/%") == FALSE && is_numeric($tab[2])) {
	calculate($tab);
} else {
	echo "Syntax Error\n";
}
?>