#!/usr/bin/php
<?php

function split_arg($str) {

	$arr = explode(':', $str);
	if (count($arr) != 2) {
		return (NULL);
	}
	return $arr;
}

if ($argc > 2) {
	$res;

	for ($i = 2; $i < $argc; $i++) {
		$arr = split_arg($argv[$i]);
		if (strcmp($arr[0], $argv[1]) == 0 && $arr[1]) {
			$res = $arr[1];
		}
	}
	if ($res) {
		echo "$res\n";
	}

}
?>