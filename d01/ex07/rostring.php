#!/usr/bin/php
<?php

function ft_split($string) {
	if ($string) {
		$string = preg_replace('!\s+!', ' ', trim($string));
		$result = explode(' ', $string);
		return $result;
	}
}

function print_rotated($arr) {
	$len = count($arr);
	for ($i = 1; $i < $len; $i++) {
		print("$arr[$i] ");
	}
	print("$arr[0]\n");
}

if ($argc > 1) {
	$first_arg = ft_split($argv[1]);
	print_rotated($first_arg);
}

?>