#!/usr/bin/php
<?php
function ft_split($string) {
	if ($string) {
		$string = preg_replace('!\s+!', ' ', trim($string));
		$result = explode(' ', $string);
		sort($result);
		return $result;
	}
}

function split_all($argc, $argv) {
	$result = array();
	for ($i = 1; $i < $argc; $i++) {
		$curr = ft_split($argv[$i]);
		$result = array_merge($result, $curr);
	}
	return $result;
}

function charcmp($a, $b) {
	switch ($a) {
	case ctype_digit($a):
		$res = ctype_digit($b) ? ($a - $b) : (ctype_alpha($b) ? 1 : -1);
		break;
	case ctype_alpha($a):
		$res = ctype_alpha($b) ? strcasecmp($a, $b) : -1;
		break;
	case ctype_alnum($a) == FALSE:
		$res = ctype_alnum($b) ? 1 : strcmp($a, $b);
		break;
	default:
		$res = 0;
		break;
	}

	return ($res);
}

function cmp($a, $b) {
	$len = min(strlen($a), strlen($b));
	for ($i = 0; $i < $len; $i++) {
		$res = charcmp($a[$i], $b[$i]);
		if ($res != 0) {
			return $res;
		}

	}
}

$result = split_all($argc, $argv);
sort($result);
usort($result, "cmp");
foreach ($result as $elem) {
	echo "$elem\n";
}

?>