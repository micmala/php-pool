<?php
/**
 * Created by PhpStorm.
 * User: mic
 * Date: 24/01/19
 * Time: 12:37
 */

class Jaime extends Lannister
{
    public function sleepWith($person)
    {
        if ($person instanceof Cersei)
            echo "With pleasure, but only in a tower in Winterfell, then." . PHP_EOL;
        else if ($person instanceof Sansa)
            echo "Let's do this." . PHP_EOL;
        else
            parent::sleepWith($person);
    }
}