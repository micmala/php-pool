<?php
/**
 * Created by PhpStorm.
 * User: mic
 * Date: 24/01/19
 * Time: 11:33
 */

class Targaryen
{
    public function resistsFire()
    {
        return False;
    }

    public function getBurned()
    {
        if ($this->resistsFire()) {
            return ("emerges naked but unharmed");
        } else {
            return ("burns alive");
        }

    }
}