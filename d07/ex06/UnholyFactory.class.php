<?php
/**
 * Created by PhpStorm.
 * User: mic
 * Date: 24/01/19
 * Time: 15:44
 */

class UnholyFactory
{
    private $squad = array();

    public function absorb($fighter)
    {
        echo "(Factory ";
        if ($fighter instanceof Fighter === False) {
            echo "can't absorb this, it's not a fighter)" . PHP_EOL;
        } else {
            if (in_array($fighter, $this->squad)) {
                echo "already absorbed";
            } else {
                echo "absorbed";
                array_push($this->squad, $fighter);
            }
            echo " a fighter of type " . $fighter->getTypeName() . ")" . PHP_EOL;
        }
    }

    public function fabricate($fighter_name)
    {
        echo "(Factory ";
        if (($fighter = $this->is_absorbed($fighter_name)) !== false) {
            echo "fabricates a fighter of type " . $fighter_name . ")" . PHP_EOL;
            return ($fighter);
        } else {
            echo "hasn't absorbed any fighter of type " . $fighter_name . ")" . PHP_EOL;
            return (NULL);
        }

    }

    private function is_absorbed($name)
    {
        foreach ($this->squad as $val) {
            if (strcmp($val->getTypeName(), $name) == 0) {
                return $val;
            }
        }
        return (False);
    }
}