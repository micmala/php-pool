<?php
/**
 * Created by PhpStorm.
 * User: mic
 * Date: 24/01/19
 * Time: 15:45
 */

abstract class Fighter
{
    protected $type_name;

    public function __construct($param)
    {
        $this->type_name = $param;
    }

    public function getTypeName()
    {
        return $this->type_name;
    }

    abstract public function fight($target);
}