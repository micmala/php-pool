<?php
/**
 * Created by PhpStorm.
 * User: mic
 * Date: 24/01/19
 * Time: 11:43
 */

class House
{
    public function introduce()
    {
        echo "House " . static::getHouseName()
            . " of " . static::getHouseSeat()
            . " : \"" . static::getHouseMotto()
            . "\""
            . PHP_EOL;
    }
}