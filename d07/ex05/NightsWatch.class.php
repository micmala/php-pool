<?php

/**
 * Created by PhpStorm.
 * User: mic
 * Date: 24/01/19
 * Time: 14:57
 */

class NightsWatch implements IFighter
{
    private $fighters = array();

    public function fight()
    {
        foreach ($this->fighters as $figher) {
            $figher->fight();
        }
    }

    public function recruit($fighter)
    {
        if ($fighter instanceof IFighter) {
            $this->fighters[] = $fighter;
        }
    }
}