<?php
/**
 * Created by PhpStorm.
 * User: mic
 * Date: 24/01/19
 * Time: 14:57
 */

interface IFighter
{
    public function fight();
}