#!/usr/bin/php
<?php
define("ERROR", "Wrong Format\n");

$months = array(
	'janvier' => '01',
	'fevrier' => '02',
	'mars' => '03',
	'avril' => '04',
	'mai' => '05',
	'juin' => '06',
	'juillet' => '07',
	'aout' => '08',
	'septembre' => '09',
	'octobre' => '10',
	'novembre' => '11',
	'decembre' => '12',
);

$week_days = array(
	'lundi' => '01',
	'mardi' => '02',
	'mercredi' => '03',
	'jeudi' => '04',
	'vendredi' => '05',
	'samedi' => '06',
	'dimanche' => '07',
);

function find_number($arr, $target) {
	foreach ($arr as $name => $nb) {
		if (preg_match("/^$name$/", lcfirst($target)) == 1) {
			return ($nb);
		}
	}
	return (FALSE);
}

function array_to_string($tab) {
	global $months, $week_days;

	if (find_number($week_days, $tab[0]) == FALSE
		|| ($mon = find_number($months, $tab[2])) == FALSE) {
		exit(ERROR);
	}
	if (strlen($tab[1]) == 1) {
		$tab[1] = '0' . $tab[1];
	}

	$arr = array($tab[3], $mon, $tab[1]);
	$time = $tab[4];
	$date_time = implode(':', $arr);
	$date_time .= ' ' . $time;
	return ($date_time);
}

date_default_timezone_set("Europe/Paris");

$pattern = "/^([a-z]{5,8})\s([0-9]{1,2})\s([a-z]{3,9})\s([0-9]{4})\s([0-5][0-9]:[0-5][0-9]:[0-5][0-9])$/i";

if ($argc > 1) {
	$arr = preg_split($pattern, $argv[1], -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
	if (count($arr) != 5) {
		exit(ERROR);
	}

	$date_time = array_to_string($arr);
	if (($val = strtotime($date_time)) == FALSE) {
		exit(ERROR);
	}
	echo $val . PHP_EOL;
}
?>