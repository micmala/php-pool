SELECT upper(last_name) AS 'NAME', first_name, price
FROM user_card AS u
INNER JOIN member AS m ON m.id_user_card = u.id_user
INNER JOIN subscription AS s ON m.id_sub = s.id_sub
WHERE s.price > 42
ORDER BY last_name, first_name
;