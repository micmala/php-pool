SELECT count(*) AS 'movies'
FROM member_history
WHERE
date(`date`) BETWEEN cast('2006-10-30' AS DATE) AND cast('2007-07-27' AS DATE)
OR (day(date(`date`)) = 24 AND month(date(`date`)) = 12)
;