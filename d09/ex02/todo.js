function checkCookie() {
    let arr = decodeURIComponent(document.cookie).split(";");

    if (arr[0] !== '')
    {
        for (let i = 0; i < arr.length; i++){
            let temp = arr[i].split("=");
            console.log(arr[i].split("="));
            addTask(temp[0]);
        }
    }

}

function readTask() {
    let task = prompt("Please enter new task");

    if (task != null && task !== "") {
        addTask(task);
    }
}
function addCookies(name) {
    document.cookie = name + "=" + name + ";" + "expires=";
}

function delCookies(name) {
    document.cookie = name + "=; expires=Thu, 01 Jan 1970 00:00:01 GMT;";
}

function addTask(taskTxt) {
    let todo = document.createElement("DIV");
    let todoTxt = document.createTextNode(taskTxt);

    todo.setAttribute("class", "task");
    todo.onclick = removeTask;
    todo.appendChild(todoTxt);
    document.getElementById("ft_list").insertBefore(todo, document.getElementById("ft_list").firstChild);

    addCookies(taskTxt);

}

function removeTask() {
    if (confirm("Do you really want to remove this task ?")) {
        delCookies(this.innerHTML);
        this.parentNode.removeChild(this);
    }
}
