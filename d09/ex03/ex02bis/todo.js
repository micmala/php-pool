$(document).ready(function(){
    let arr = decodeURIComponent(document.cookie).split(";");

    if (arr[0] !== '')
    {
        for (let i = 0; i < arr.length; i++){
            let temp = arr[i].split("=");
            console.log(arr[i].split("="));
            addTask(temp[0]);
        }
    }

});

function readTask() {
    let task = prompt("Please enter new task");

    if (task != null && task !== "") {
        addTask(task);
    }
}

function addTask(taskTxt) {
    $('#ft_list').prepend($('<div class="task">' + taskTxt + '</div>').click(removeTask));
    addCookies(taskTxt);

}

function removeTask() {
    if (confirm("Do you really want to remove this task ?")) {
        delCookies(this.innerHTML);
        this.parentNode.removeChild(this);
    }
}

function addCookies(name) {
    document.cookie = name + "=" + name + ";" + "expires=";
}

function delCookies(name) {
    document.cookie = name + "=; expires=Thu, 01 Jan 1970 00:00:01 GMT;";
}
