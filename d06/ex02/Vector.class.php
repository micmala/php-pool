<?php

require_once "Color.class.php";
require_once "Vertex.class.php";

class Vector
{

    public static $verbose = false;

    public static function doc()
    {
        return (file_get_contents("Vector.doc.txt"));
    }

    private $_x;
    private $_y;
    private $_z;
    private $_w = 0.0;

    function __construct($argv)
    {
        if (array_key_exists('orig', $argv) === false)
            $argv['orig'] = new Vertex(array('x' => 0, 'y' => 0, 'z' => 0));
        $dest = $argv['dest'];
        $orig = $argv['orig'];

        $this->_x = $dest->getX() - $orig->getX();
        $this->_y = $dest->getY() - $orig->getY();
        $this->_z = $dest->getZ() - $orig->getZ();

        if (self::$verbose) {
            echo $this->__toString() . " constructed" . PHP_EOL;
        }
        return;
    }

    function __destruct()
    {
        if (self::$verbose) {
            echo $this->__toString() . " destructed" . PHP_EOL;
        }
        return;
    }

    public function __toString()
    {
        return sprintf("Vector( x:%0.2f, y:%0.2f, z:%0.2f, w:%0.2f )", $this->_x, $this->_y, $this->_z, $this->_w);
    }

    /**
     * @return mixed
     */
    public function getX()
    {
        return $this->_x;
    }

    /**
     * @return mixed
     */
    public function getY()
    {
        return $this->_y;
    }

    /**
     * @return mixed
     */
    public function getZ()
    {
        return $this->_z;
    }

    /**
     * @return float
     */
    public function getW()
    {
        return $this->_w;
    }


    public function magnitude()
    {
        return (float)sqrt(($this->_x * $this->_x) + ($this->_y * $this->_y) + ($this->_z * $this->_z));
    }

    public function normalize()
    {
        $length = $this->magnitude();
        if ($length == 1)
            return (clone $this);
        return (new Vector(array('dest' => new Vertex(array(
            'x' => $this->_x / $length,
            'y' => $this->_y / $length,
            'z' => $this->_z / $length
        )))));
    }

    public function add(Vector $rhs)
    {
        return (new Vector(array('dest' => new Vertex(array(
            'x' => $this->_x + $rhs->_x,
            'y' => $this->_y + $rhs->_y,
            'z' => $this->_z + $rhs->_z
        )))));
    }

    public function sub(Vector $rhs)
    {
        return (new Vector(array('dest' => new Vertex(array(
            'x' => $this->_x - $rhs->_x,
            'y' => $this->_y - $rhs->_y,
            'z' => $this->_z - $rhs->_z
        )))));
    }

    public function opposite()
    {
        return ($this->scalarProduct(-1));
    }

    public function scalarProduct($k)
    {
        return (new Vector(array('dest' => new Vertex(array(
            'x' => $this->_x * $k,
            'y' => $this->_y * $k,
            'z' => $this->_z * $k
        )))));
    }

    /**
     * @param Vector $rhs
     * @return float|int
     */
    public function dotProduct(Vector $rhs)
    {
        return ($this->_x * $rhs->_x + $this->_y * $rhs->_y + $this->_z * $rhs->_z);
    }

    public function cos(Vector $rhs)
    {
        return ($this->dotProduct($rhs) / ($this->magnitude() * $rhs->magnitude()));
    }

    public function crossProduct(Vector $rhs)
    {
        return (new Vector(array('dest' => new Vertex(array(
            'x' => $this->_y * $rhs->getZ() - $this->_z * $rhs->getY(),
            'y' => $this->_z * $rhs->getX() - $this->_x * $rhs->getZ(),
            'z' => $this->_x * $rhs->getY() - $this->_y * $rhs->getX()
        )))));
    }

}