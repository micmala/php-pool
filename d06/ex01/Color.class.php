<?php
/**
 * Created by PhpStorm.
 * User: mic
 * Date: 23/01/19
 * Time: 12:17
 */

class Color
{

    public static function doc()
    {
        return (file_get_contents("Color.doc.txt"));
    }

    public static $verbose = false;

    public $red;
    public $green;
    public $blue;

    function __construct($argv)
    {
        if (array_key_exists('rgb', $argv)) {
            $rgb = intval($argv['rgb']);
            $this->red = ($rgb >> 8 * 2) & 0xff;
            $this->green = ($rgb >> 8) & 0xff;
            $this->blue = $rgb & 0xff;
        } else {
            $this->red = intval($argv['red']);
            $this->green = intval($argv['green']);
            $this->blue = intval($argv['blue']);
        }
        if (self::$verbose)
            echo $this->__toString() . " constructed." . PHP_EOL;
        return;
    }

    function __destruct()
    {
        if (self::$verbose)
            echo $this->__toString() . " destructed." . PHP_EOL;
        return;
    }

    function add(Color $rhs)
    {
        return (new Color(array(
                'red' => $this->red + $rhs->red,
                'green' => $this->green + $rhs->green,
                'blue' => $this->blue + $rhs->blue
            )
        ));
    }

    function sub(Color $rhs)
    {
        return (new Color(array(
                'red' => $this->red - $rhs->red,
                'green' => $this->green - $rhs->green,
                'blue' => $this->blue - $rhs->blue
            )
        ));
    }

    function mult($f)
    {
        return (new Color(array(
                'red' => $this->red * $f,
                'green' => $this->green * $f,
                'blue' => $this->blue * $f
            )
        ));
    }

    public function __toString()
    {
        return sprintf("Color( red: %3d, green: %3d, blue: %3d )", $this->red, $this->green, $this->blue);
    }


}
