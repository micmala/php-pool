<?php

require_once 'Color.class.php';

class Vertex
{

    public static $verbose = false;

    /**
     * Vertex constructor.
     * @param $_x
     * @param $_y
     * @param $_z
     * @param float $_w
     */
    public function __construct($argv)
    {
        $this->_x = $argv['x'];
        $this->_y = $argv['y'];
        $this->_z = $argv['z'];
        if (array_key_exists('w', $argv)) {
            $this->_w = $argv['w'];
        }
        if (array_key_exists('color', $argv)) {
            $this->_color = $argv['color'];
        } else {
            $this->_color = new Color(array('red' => 255, 'green' => 255, 'blue' => 255));
        }

        if (self::$verbose) {
            echo $this->__toString() . " constructed" . PHP_EOL;
        }
        return;
    }

    function __destruct()
    {
        if (self::$verbose) {
            echo $this->__toString() . " destructed" . PHP_EOL;
        }
        return;
    }

    public static function doc()
    {
        return (file_get_contents("Vertex.doc.txt"));
    }

    private $_x;
    private $_y;
    private $_z;
    private $_w = 1.0;
    private $_color;

    public function __toString()
    {
        $str = sprintf("Vertex( x: %0.2f, y: %0.2f, z:%0.2f, w:%0.2f", $this->_x, $this->_y, $this->_z, $this->_w);
        if (self::$verbose)
            $str .= ", " . $this->_color->__toString();
        $str .= " )";
        return $str;
    }


    /**
     * @return mixed
     */
    public function getX()
    {
        return $this->_x;
    }

    /**
     * @param mixed $x
     */
    public function setX($x)
    {
        $this->_x = $x;
    }

    /**
     * @return mixed
     */
    public function getY()
    {
        return $this->_y;
    }

    /**
     * @param mixed $y
     */
    public function setY($y)
    {
        $this->_y = $y;
    }

    /**
     * @return mixed
     */
    public function getZ()
    {
        return $this->_z;
    }

    /**
     * @param mixed $z
     */
    public function setZ($z)
    {
        $this->_z = $z;
    }

    /**
     * @return float
     */
    public function getW()
    {
        return $this->_w;
    }

    /**
     * @param float $w
     */
    public function setW($w)
    {
        $this->_w = $w;
    }

    /**
     * @return Color
     */
    public function getColor()
    {
        return $this->_color;
    }

    /**
     * @param Color $color
     */
    public function setColor($color)
    {
        $this->_color = $color;
    }

}